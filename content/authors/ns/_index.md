+++
name = "Noah Summers"
aliases = ["/about"]
bio = "Software developer disenchanted with paradigms, patterns, and syntax highlighting (subject to change). Lives in Dallas, Texas, with his wife and two children."

[[hubs]]
name = "GitHub"
icon = "github"
url = "https://github.com/noahsummers"

[[hubs]]
name = "Twitter"
icon = "twitter"
url = "https://twitter.com/rewritable_me"

[[hubs]]
name = "DEV"
icon = "message-square"
url = "https://dev.to/noahsummers"

[[hubs]]
name = "Email"
icon = "mail"
url  = "mailto:ns@rewritable.me"
+++
